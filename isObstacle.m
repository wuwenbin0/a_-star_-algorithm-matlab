function flag = isObstacle( m, obstacle )
%判断节点m是否在障碍物区域
if m(2) >= 100 ||  m(2) <= 0 || m(1) <= 0 || m(1) >= 100 %超出外部区域
   % disp('超出范围')
    flag = true;
    return;
end
for io = 1 : length(obstacle(:,1))
    dis = 2;
    x1 = obstacle(io, 1) - dis;       %矩形左下角x坐标
    y1 = obstacle(io, 2) - dis;       %矩形左下角y坐标
    len = obstacle(io, 3) + 2*dis;   %矩形的长度
    hei = obstacle(io, 4) + 2*dis;   %矩形的高度
    
    xv = [x1,     x1,       x1 + len,  x1 + len];
    yv = [y1,  y1 + hei,       y1+hei,     y1];   
    [in, ~] = inpolygon(m(1), m(2), xv, yv);
    if in   %如果in为逻辑1，代表点在内部或边缘
        flag = true;
        %disp('在障碍物内部')
        return;
    end
end
%disp('在障碍物外部')
flag = false;
end

clear;
%clc;
close all;

%画出地图背景网格线和目标点、起始点
%PlotGrid(map);

obstacle = [0,30,8,5; 0,50,8,5; 
            10,0,5,18; %S6
            30,50,20,5; 45,55,5,20; 
            20,70,5,20; %S10
            75,30,17,5;90,0,5,20;  %S3
            65,80,5,20; %S5
            63,0,5,17;  
            37,10,5,20; 25,30,17,5; 50,30,13,5;  %S7\S2
            70,50,20,5; 78,55,5,20;   %右上T型
];
workstation = [40 62; 50 25; 85 25; 87 62; 60 90;
               18 10; 30 25; 5 62;  72 62; 27 80;
];
label = [40 60; 50 28; 87 25; 87 65; 60 90;
         18 10; 30 25; 5 65;  72 62; 29 80;
];


% obstacle = [15,20,20,3; 15,23,3,15; 32,23,3,15;];
hold on;
%axis off
%画出障碍区域
for i = 1 : length(obstacle(:,1))
    rectangle('Position',obstacle(i,:),'FaceColor','black');
end
displayStation(label);
%%
axis off
map.XYMAX=100; %%代表我们要画一个地图的长和宽
axis([0,map.XYMAX,0,map.XYMAX]);

map.start = [1,98];  
map.goal=workstation(2,:);  
 %pathD2 = AStar(obstacle,map); %A*算法得到路径
%path = smooth(path);
load pathD2 pathD2
% p1 = plot(pathD2(:, 1), pathD2(:, 2),'k');

map.start = workstation(2,:);  
map.goal=workstation(3,:);  
%path23 = AStar(obstacle,map); %A*算法得到路径
load path23 path23
% plot(path23(:, 1), path23(:, 2),'k');

map.start = workstation(3,:);  
map.goal=workstation(4,:);  
%path34 = AStar(obstacle,map); %A*算法得到路径
%path = smooth(path);
%save path34 path34
load path34 path34 
% plot(path34(:, 1), path34(:, 2),'k');

map.start = workstation(4,:);  
map.goal=workstation(9,:);  
%path49 = AStar(obstacle,map); %A*算法得到路径
%save path49 path49
load path49 path49
%path = smooth(path);
% plot(path49(:, 1), path49(:, 2),'k');

map.start = workstation(9,:);
map.goal=[1,98];  
%path9D = AStar(obstacle,map); %A*算法得到路径
%save path9D path9D
load path9D path9D
%path = smooth(path);
% plot(path9D(:, 1), path9D(:, 2),'k');

%2号AGV
map.start = [1,98];  
map.goal=workstation(5,:);   
%  pathD5 = AStar(obstacle,map); %A*算法得到路径
%  save pathD5 pathD5
load pathD5 pathD5
%path = smooth(path);
% p2 = plot(pathD5(:, 1), pathD5(:, 2),'k--');

map.start = workstation(5,:);  
map.goal=workstation(1,:);  
%path51 = AStar(obstacle,map); %A*算法得到路径
%save path51 path51
load path51 path51
%path = smooth(path);
% plot(path51(:, 1), path51(:, 2),'k--');

map.start = workstation(1,:);  
map.goal=[1,98];   
%  path1D = AStar(obstacle,map); %A*算法得到路径
%  save path1D path1D
load path1D path1D
%path = smooth(path);
%  plot(path1D(:, 1), path1D(:, 2),'k--');

%3号AGV
map.start = [1,98];  
map.goal=workstation(7,:);   
%   pathD7 = AStar(obstacle,map); %A*算法得到路径
%   save pathD7 pathD7
load pathD7 pathD7
%path = smooth(path);
% plot(pathD7(:, 1), pathD7(:, 2),'k:');

map.start = workstation(7,:);  
map.goal=workstation(6,:);  
% path76 = AStar(obstacle,map); %A*算法得到路径
% save path76 path76
load path76 path76
%path = smooth(path);
% plot(path76(:, 1), path76(:, 2),'k:');

map.start = workstation(6,:);  
map.goal=[1,98];   
%  path6D = AStar(obstacle,map); %A*算法得到路径
%  save path6D path6D
load path6D path6D
%path = smooth(path);
% p3 = plot(path6D(:, 1), path6D(:, 2),'k:');

%4号AGV
map.start = [1,98];  
map.goal=workstation(10,:);   
%   pathD10 = AStar(obstacle,map); %A*算法得到路径
pathD10 = path1D(1:40, :);
  save pathD10 pathD10
load pathD10 pathD10
%path = smooth(path);
%  plot(pathD10(:, 1), pathD10(:, 2),'k-.');

map.start = workstation(10,:);  
map.goal=workstation(8,:);  
%  path108 = AStar(obstacle,map); %A*算法得到路径
%  save path108 path108
load path108 path108
%path = smooth(path);
% plot(path108(:, 1), path108(:, 2),'k-.');

map.start = workstation(8,:);  
map.goal=[1,98];   
%  path8D = AStar(obstacle,map); %A*算法得到路径
%  save path8D path8D
load path8D path8D
%path = smooth(path);
% p4 = plot(path8D(:, 1), path8D(:, 2),'k-.');
% legend
%  legend([p1, p2, p3, p4],{'AGV1','AGV2','AGV3','AGV4'},'Location','northeast','FontSize',8);
 axis off
 
 route1 = [flip(pathD2);flip(path23); flip(path34); flip(path49); flip(path9D)];
% p1 =  plot(route1(:,1), route1(:,2),'k');
  route2 = [flip(pathD5);flip(path51); flip(path1D)];
% p2 =  plot(route2(:,1), route2(:,2),'g--');
 route3 = [flip(pathD7);flip(path76); flip(path6D)];
% p3 =  plot(route3(:,1), route3(:,2),'r:');
route4 = [flip(pathD10);flip(path108);];
  %route4 = [flip(pathD10);flip(path108); flip(path8D)];
p4 =  plot(route4(:,1), route4(:,2),'c-.');
 legend([p1, p2, p3, p4],{'AGV1','AGV2','AGV3','AGV4'},'Location','northeast','FontSize',8);
  
function path = AStar(obstacle,map)

path=[];        %用于存储路径
open=[];        %OpenList
close = [];       %CloseList
findFlag=false; %findFlag用于判断while循环是否结束

%================1.将起始点放在Openlist中======================
%open变量每一行  [节点坐标，代价值F=G+H,代价值G,父节点坐标]
open =[map.start(1), map.start(2) , 0 + h(map.start,map.goal) , 0 , map.start(1) , map.start(2)];
next=MotionModel();  %更新状态--下一步的八个点

%=======================2.重复以下过程==============================
while ~findFlag
    if isempty(open(:,1))  %首先判断是否达到目标点，或无路径
        disp('No path to goal!!');
        return;
    end
    
    %判断目标点是否出现在open列表中
    [isopenFlag,Id] = isopen(map.goal, open);
    if isopenFlag
        disp('Find Goal!!');
        close = [open(Id,:);close];  %加入close列表
        findFlag=true;
        break;
    end

    [~, Index] = min(open(:,3)); %找到F值最小那个编号
    current = open(Index,:);
    %FillPlot(current,'g')
    close = [current; close];    %将当前节点加入close列表
    
    open(Index,:)=[];            %将当前节点从open列表移除
    
    %--------------------c.对当前节点周围的8个相邻节点，算法的主体：------------------------
    for in = 1 : length(next(:,1))
        %获得相邻节点的坐标,代价值F先等于0,代价值G先等于0  ,后面两个值是其父节点的坐标值，暂定为零(因为暂时还无法判断其父节点坐标是多少)
        m = [current(1,1) + next(in, 1) , current(1,2)+next(in,2) , 0 , 0 , 0 ,0]; 
        m(4) = current(1,4) + next(in,3);     %计算该节点的G值
        m(3) = m(4) + h(m(1:2), map.goal);    %计算该节点的F值
        
        %>如果它不可达，跳过
        if isObstacle(m,obstacle)
            continue;
        end
        
        %flag == 3：相邻节点  在Openlist中   targetInd = open中行号
        [flag, targetInd] = FindList(m, open, close);
        
        %flag == 1：相邻节点  在Closelist中  targetInd = close中行号
        if flag==1 
            continue;
            
        %flag为2：相邻节点  不在Openlist中 targetInd = []，加入Openlist,并把当前节点设置为它的父节点
        elseif flag==2 
            m(5:6) = [current(1),current(2)];%将当前节点作为其父节点
            open = [open;m];%将此相邻节点加放openlist中
            
        %>>剩下的情况就是它在Openlist中，检查由当前节点到相邻节点是否更好，如果更好则将当前节点设置为其父节点，并更新F,G值；否则不操作    
        else
            %由当前节点到达相邻节点更好(targetInd是此相邻节点在open中的行号 此行的第3列是代价函数F值)
            if m(3) < open(targetInd,3)
                %更好，则将此相邻节点的父节点设置为当前节点，否则不作处理
                m(5:6)=[current(1,1),current(1,2)];%将当前节点作为其父节点
                open(targetInd,:) = m;%将此相邻节点在Openlist中的数据更新
            end
        end
    end
   
    %绘制节点close和open节点
%      FillPlot(close,'r');
      
    
end

%追溯路径
path=GetPath(close,map.start);


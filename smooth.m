function [newPath] = smooth(path)
%UNTITLED 此处显示有关此函数的摘要
%   此处显示详细说明
dif = diff(path(:,2)) ./ diff(path(:,1));
for i = 2 : length(dif)
    if dif(i) ~= dif(i - 1)
        path(i, 1) = -1;
    end
end
index = find(path(:,1)== -1);
for i = 1 : length(index)
    a = index(i);
    if a+6 > length(path) || a - 8 < 1
        continue;
    end
    if path(a-1, 1)== path(a-2, 1)  
        x2 = [path(a-2,1); path(a+1 :a+2,1)];
        y2 = [path(a-2,2); path(a+1 :a+2,2)];
    else
        x2 = [path(a-2:a-1,1); path(a+2,1)];
        y2 = [path(a-2:a-1,2); path(a+2,2)];
    end
    xx = min(x2) : 0.01 :max(x2);
    path(min(x2):max(x2),1) = -1;
    yy=interp1(x2,y2,xx,'pchip'); %3次样条插值
    new = [xx;yy]';
    path = [path; new];
   % path(:,1) = [path(:,1); xx];
   % path(:,2) = [path(:,2); yy];
   
end

 index = find(path(:,1)== -1);
 path(index,:) = [];
 newPath = sort(path,1);
end

